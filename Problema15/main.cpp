#include <iostream>

using namespace std;

void calcularPosicion(int *rectangulo){
    /* Funcion que calcula los valores correspondientes del arreglo y los asigna*/

    rectangulo[0] =*(rectangulo);   //Valor de X inicial
    rectangulo[1] =*(rectangulo+1); //Valor de Y inicial
    rectangulo[2] =(*(rectangulo)+ *(rectangulo+2));    //Valor de X final al restar el ancho al valor de X inicial
    rectangulo[3] =(*(rectangulo+1)- *(rectangulo+3));  //Valor de Y final al restar el alto al valor de Y inicial
}

void interseccion(int *posicionA,int *posicionB,int *posicionC) {

    /* Funcion que por medio de algunos casos especificos, encuentra la interseccion de dos rectangulos, retornando los valores del tercer triangulo
        asignando posiciones de A o B al arreglo C de acuerdo al caso especifico*/

    //Posicion[0]=Coordenada X ; Posicion[1]= Coordenada Y ; Posicion [2]= Ancho del rectangulo ; Posicion [3]= Alto del rectangulo

    //2
    if (posicionA[2]<=posicionB[2] && posicionA[3] >= posicionB[3]){    //Si el ancho de A es menor o igual que el de B y el alto de A es mayor que el de B
        if (posicionA[0]>= posicionB[0] && posicionA[1] >= posicionB[1]){
            posicionC[0]=posicionA[0];
            posicionC[1]=posicionB[1];
            posicionC[2]=posicionA[2];
            posicionC[3]=posicionB[3];
        }
    }

    //1
        if (posicionB[2]<=posicionA[2] && posicionA[0]<=posicionB[0]){  //Si ancho de B es menor o igual al de A
            if (posicionA[1]<=posicionB[1]){
                if (posicionA[3]<=posicionB[3]){  //Si alto de A es menor o igual al de B
                        posicionC[0]=posicionB[0];    //X de C será el de B
                        posicionC[1]=posicionA[1];    // Y de C sera el de A
                        posicionC[2]=posicionB[2];    //El ancho de C sera el de B
                        posicionC[3]=posicionB[3];    //El alto de C sera el de B
                    }
        }
}
        //6
        if (posicionA[0]<= posicionB[0] && posicionA[1] <= posicionB[1]){
            if ( posicionA[2]>= posicionB[2] && posicionA[3]<= posicionB[3]){
                posicionC[0]=posicionB[0];
                posicionC[1]=posicionB[1];
                posicionC[2]=posicionB[3]-posicionA[3];
                posicionC[3]=posicionA[2]-posicionB[2];
            }
        }



        if(posicionA[0]<=posicionB[0] && posicionA[2]<=posicionB[2]){   //Si el ancho de A es menor que el de B
                    if( posicionA[1]>=posicionB[1] && posicionA[3]<=posicionB[3]){      // Si el alto de A es menor que el de B
                        posicionC[0]=posicionB[0];
                        posicionC[1]=posicionB[1];
                        posicionC[2]=posicionA[2];
                        posicionC[3]=posicionB[3];

                    }

                }
                if(posicionB[0]<=posicionA[0] && posicionB[1]>=posicionA[1]){
                    if(posicionB[2]<=posicionA[2] && posicionB[3]<=posicionA[3]){       //El ancho y el alto de B es menor que el de A
                        posicionC[0]=posicionA[0];
                        posicionC[1]=posicionA[1];
                        posicionC[2]=posicionB[2];
                        posicionC[3]=posicionA[3];


                    }
                }
                if(posicionA[0]>=posicionB[0] && posicionA[2]>=posicionB[2]){       //El ancho de A es mayor que el de B
                    if( posicionA[1]>=posicionB[1] && posicionA[3]<=posicionB[3]){  //El alto de A es menor que el de B
                        posicionC[0]=posicionA[0];
                        posicionC[1]=posicionB[1];
                        posicionC[2]=posicionB[2];
                        posicionC[3]=posicionB[3];

                    }

                }
                //6
                if(posicionB[0]>=posicionA[0] && posicionB[1]>=posicionA[1]){
                    if(posicionB[2]>=posicionA[2] && posicionB[3]<=posicionA[3]){ // El ancho de B es mayor que el de A y el alto de B es menor que el de A
                        posicionC[0]=posicionB[0];
                        posicionC[1]=posicionB[1];
                        posicionC[2]=posicionB[3]-posicionA[3];
                        posicionC[3]=posicionA[2]-posicionB[2];

                    }
                }




              //3
        if (posicionA[1]>=posicionB[1]) {     //Si la posicion Y de A es mayor o igual que el de B
            if ((posicionA[0]>=posicionB[0])&& (posicionB[2]<=posicionA[2])){   //Si la posicion X es mayor o igual a la de B, y El ancho de B es menor o igual que el de A
                if (posicionA[1]>posicionB[3] && posicionA[3]<=posicionB[3]){   //Si la altura de A es menor o igual que la de B
                    posicionC[0]=posicionA[0];
                    posicionC[1]=posicionB[1];
                    posicionC[2]=posicionB[2];
                    posicionC[3]=posicionA[3];
                }
            }
            else if  (((posicionA[0]<=posicionB[0])&& (posicionB[2]>=posicionA[2]))) {  //Si la posicion X es menor o igual a la de B, y El ancho de B es mayor o igual que el de A
                if (posicionA[2]>posicionB[0] && posicionA[3]<=posicionB[3]){  //Si la altura de A es menor o igual que la de B
                    posicionC[0]=posicionB[0];
                    posicionC[1]=posicionB[1];
                    posicionC[2]=posicionA[2];
                    posicionC[3]=posicionA[3];
                }
            }
        }

        //5

        if (posicionA[0]>=posicionB[0] &&  posicionA[2]<=posicionB[2]) {    //Si el valor de X en A es mayor que el de B y el ancho de A es menor que el de B
            if ((posicionA[1]<=posicionB[1])&& (posicionB[3]<=posicionA[3])){   //Si el valor de Y es menor o igual que el de B y el alto de B es menor que el de A
                    posicionC[0]=posicionA[0];
                    posicionC[1]=posicionA[1];
                    posicionC[2]=posicionA[2];
                    posicionC[3]=posicionA[3];
            }
        }
            else if  (posicionB[0]>=posicionA[0] &&  posicionB[2]<=posicionA[2]) {      //Si el valor de X en B es mayor que en A y el ancho de B es menor o giual que el de A
            if (posicionB[1]<=posicionA[1] && posicionA[3]<=posicionB[3]){   //Si el valor de Y en B es menor o igual que en A y el alto de A es menor o igual que B
                    posicionC[0]=posicionB[0];
                    posicionC[1]=posicionB[1];
                    posicionC[2]=posicionB[2];
                    posicionC[3]=posicionB[3];
            }
        }
     }

int main()
{
    /* Este programa calcula la interseccion de dos triangulos dando los resultados en un arreglo C, en donde se toman los siguientes datos
    de los dos primeros triangulos: Coordenadas X, Y, el ancho del triangulo y el alto. */

    int rectangulo_a[4],rectangulo_b[4],rectangulo_c[4];

    cout << "Ingrese la posicion X para el rectangulo a: "<<endl;
    cin >> rectangulo_a[0];
    cout << "Ingrese la posicion Y para el rectangulo a: "<<endl;
    cin >> rectangulo_a[1];
    cout << "Ingrese el ancho del rectangulo a: "<<endl;
    cin >> rectangulo_a[2];
    cout << "Ingrese el alto del rectangulo a: "<<endl;
    cin >> rectangulo_a[3];
    cout << "Ingrese la posicion X para el rectangulo b: "<<endl;
    cin >> rectangulo_b[0];
    cout << "Ingrese la posicion Y para el rectangulo b: "<<endl;
    cin >> rectangulo_b[1];
    cout << "Ingrese el ancho del rectangulo b: "<<endl;
    cin >> rectangulo_b[2];
    cout << "Ingrese el alto del rectangulo b: "<<endl;
    cin >> rectangulo_b[3];

    calcularPosicion(rectangulo_a);
    calcularPosicion(rectangulo_b);

    interseccion(rectangulo_a, rectangulo_b, rectangulo_c);  //Va a la funcion interseccion con los 3 arreglos ya determinados y el C a determinar

    rectangulo_c[2]=abs(rectangulo_c[0]-rectangulo_c[2]); //Se le restan el ancho al X y el alto a Y para saber la medida
    rectangulo_c[3]=abs(rectangulo_c[1]-rectangulo_c[3]);

    cout<<"X: "<<rectangulo_c[0]<<"  Y: "<<rectangulo_c[1]<<"  Ancho: "<<rectangulo_c[2]<<"  Alto: "<<rectangulo_c[3]<<endl;


}

